﻿namespace Engine {
    public enum Targetable {
        Ally, Enemy, Both
    }
}