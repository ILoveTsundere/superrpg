﻿namespace Engine {
    public enum BuffType {
        Regeneration,
        PlusStat,
        PlusElementalRes,
        PlusPhysicalRes,
        PlusMagicalRes
        }
    }