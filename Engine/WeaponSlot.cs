﻿namespace Engine {
    public enum WeaponSlot {
        TwoHand,
        Shield,
        OneHand
    }
}