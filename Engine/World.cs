﻿using System.Collections.Generic;

namespace Engine {
    public static class World {

        public static readonly List<Item> Items = new List<Item>();
        public static readonly List<NPC> NPCs = new List<NPC>();
        public static readonly List<Quest> Quests = new List<Quest>();
        public static readonly Dungeon WooTown;
        public static readonly List<Location> Locations = new List<Location>();
        public static readonly List<Enemy> Enemies = new List<Enemy> (); 

        public const int UNSELLABLE_ITEM_ID = -1;
        public const int ITEM_ID_RUSTY_SWORD = 1;
        public const int ITEM_ID_RAT_TAIL = 2;
        public const int ITEM_ID_PIECE_OF_FUR = 3;
        public const int ITEM_ID_SNAKE_FANG = 4;
        public const int ITEM_ID_SNAKESKIN = 5;
        public const int ITEM_ID_CLUB = 6;
        public const int ITEM_ID_HEALING_POTION = 7;
        public const int ITEM_ID_SPIDER_FANG = 8;
        public const int ITEM_ID_SPIDER_SILK = 9;
        public const int ITEM_ID_ADVENTURER_PASS = 10;

        public const int BOB_THE_RAT_CATCHER_ID = 0;
        public const int FARMER_ID = 4;
        public const int ALCHEMIST_ID = 5;
        public const int NPC_ID_RAT = 1;
        public const int NPC_ID_SNAKE = 2;
        public const int NPC_ID_GIANT_SPIDER = 3;

        public const int QUEST_ID_CLEAR_ALCHEMIST_GARDEN = 1;
        public const int QUEST_ID_CLEAR_FARMERS_FIELD = 2;

        public const int DUNGEON_ID_WOOTOWN = 0;

        public const int LOCATION_ID_HOME = 1;
        public const int LOCATION_ID_TOWN_SQUARE = 2;
        public const int LOCATION_ID_GUARD_POST = 3;
        public const int LOCATION_ID_ALCHEMIST_HUT = 4;
        public const int LOCATION_ID_ALCHEMISTS_GARDEN = 5;
        public const int LOCATION_ID_FARMHOUSE = 6;
        public const int LOCATION_ID_FARM_FIELD = 7;
        public const int LOCATION_ID_BRIDGE = 8;
        public const int LOCATION_ID_SPIDER_FIELD = 9;

        static World ( ) {
            WooTown = new Dungeon ( DUNGEON_ID_WOOTOWN, "Woo Town"  );
            PopulateItems();
            PopulateNPCs();
            PopulateQuests();
            PopulateLocations();
            }

        private static void PopulateItems ( ) {
            Items.Add(new Weapon(ITEM_ID_RUSTY_SWORD, "Rusty sword", "Rusty swords", 0, 5, WeaponSlot.OneHand, "It's all worn out.", 5));
            Items.Add(new Item(ITEM_ID_RAT_TAIL, "Rat tail", "Rat tails", "It's a rat tail.", 1));
            Items.Add(new Item(ITEM_ID_PIECE_OF_FUR, "Piece of fur", "Pieces of fur", "Some fur", 1));
            Items.Add(new Item(ITEM_ID_SNAKE_FANG, "Snake fang", "Snake fangs", "It was a pain taking these out", 1));
            Items.Add(new Item(ITEM_ID_SNAKESKIN, "Snakeskin", "Snakeskins", "Sssnakeskin", 2 ));
            Items.Add(new Weapon(ITEM_ID_CLUB, "Club", "Clubs", 3, 10, WeaponSlot.OneHand, "It's clubbin' time.", 8));
            Items.Add(new HealingPotion(ITEM_ID_HEALING_POTION, "Healing potion", "Healing potions", 5, "Heal yourself for 5", 3));
            Items.Add(new Item(ITEM_ID_SPIDER_FANG, "Spider fang", "Spider fangs", "Spider fang", 1));
            Items.Add(new Item(ITEM_ID_SPIDER_SILK, "Spider silk", "Spider silks", "Spider silk taken from spiders", 1));
            Items.Add(new Item(ITEM_ID_ADVENTURER_PASS, "Adventurer pass", "Adventurer passes", "You need this", UNSELLABLE_ITEM_ID));
            }

        private static void PopulateNPCs ( ) {
            Enemy rat = new Enemy(NPC_ID_RAT, "Rat", "A Scary rat!", 3, 5, 5, 5, 3, 5, 10, 3, 3);
            rat.LootTable.Add(new LootItem(ItemByID(ITEM_ID_RAT_TAIL), 75, false));
            rat.LootTable.Add(new LootItem(ItemByID(ITEM_ID_PIECE_OF_FUR), 75, true));

            Enemy snake = new Enemy(NPC_ID_SNAKE, "Snake", "Sssssnake.", 3, 5, 3, 5, 5, 5, 10, 3, 3);
            snake.LootTable.Add(new LootItem(ItemByID(ITEM_ID_SNAKE_FANG), 75, false));
            snake.LootTable.Add(new LootItem(ItemByID(ITEM_ID_SNAKESKIN), 75, true));

            Enemy giantSpider = new Enemy(NPC_ID_GIANT_SPIDER, "Giant spider", "Aaaahh!", 5, 20, 5, 4, 4, 5, 40, 10, 10);
            giantSpider.LootTable.Add(new LootItem(ItemByID(ITEM_ID_SPIDER_FANG), 75, true));
            giantSpider.LootTable.Add(new LootItem(ItemByID(ITEM_ID_SPIDER_SILK), 25, false));

            NPC bobTheRatCatcherVendor = new NPC(BOB_THE_RAT_CATCHER_ID, "Bob the Rat-Catcher", "Catches rats in his spare time", 100, 100, 50, 50, 10, 50);
            bobTheRatCatcherVendor.AddItemToInventory(ItemByID(ITEM_ID_PIECE_OF_FUR), 5);
            bobTheRatCatcherVendor.AddItemToInventory(ItemByID(ITEM_ID_RAT_TAIL), 10);

            NPC farmer = new NPC(FARMER_ID, "Farmer", "Would farm more, but rats are annoying.", 100, 100, 50, 50, 10, 50);

            NPC alchemist = new NPC(ALCHEMIST_ID, "Farmer", "Would farm more, but rats are annoying.", 100, 100, 50, 50, 10, 100);
            alchemist.AddItemToInventory ( ItemByID ( ITEM_ID_HEALING_POTION ), 10 );


            Enemies.Add(rat);
            Enemies.Add(snake);
            Enemies.Add(giantSpider);
            NPCs.Add ( bobTheRatCatcherVendor );
        }
        private static void PopulateQuests ( ) {
            Quest clearAlchemistGarden = new Quest(QUEST_ID_CLEAR_ALCHEMIST_GARDEN,
            "Clear the alchemist's garden",
            "Kill rats in the alchemist's garden and bring back 3 rat tails.\nYou will receive a healing potion and 10 gold pieces.", 20, 10);

            clearAlchemistGarden.QuestCompletionItems.Add(new QuestCompletionItem(ItemByID(ITEM_ID_RAT_TAIL), 3));

            clearAlchemistGarden.RewardItem = ItemByID(ITEM_ID_HEALING_POTION);

            Quest clearFarmersField = new Quest(QUEST_ID_CLEAR_FARMERS_FIELD,
            "Clear the farmer's field",
            "Kill snakes in the farmer's field and bring back 3 snake fangs.\nYou will receive an adventurer's pass and 20 gold pieces.", 20, 20);

            clearFarmersField.QuestCompletionItems.Add(new QuestCompletionItem(ItemByID(ITEM_ID_SNAKE_FANG), 3));

            clearFarmersField.RewardItem = ItemByID(ITEM_ID_ADVENTURER_PASS);

            Quests.Add(clearAlchemistGarden);
            Quests.Add(clearFarmersField);
            }

        public static void PopulateLocations ( ) {
            // Create each location
            Location home = new Location(LOCATION_ID_HOME, "Home",
            "Your house. You really need to clean up the place.");

            Location townSquare = new Location(LOCATION_ID_TOWN_SQUARE,
            "Town square", "You see a fountain.");
            townSquare.NPCAvailableHere = NPCByID ( BOB_THE_RAT_CATCHER_ID );

            Location alchemistHut = new Location(LOCATION_ID_ALCHEMIST_HUT,
            "Alchemist's hut", "There are many strange plants on the shelves.");
            alchemistHut.QuestAvailableHere = QuestByID(QUEST_ID_CLEAR_ALCHEMIST_GARDEN);

            Location alchemistsGarden = new Location(LOCATION_ID_ALCHEMISTS_GARDEN,
            "Alchemist's garden", "Many plants are growing here.");
            alchemistsGarden.NPCAvailableHere = NPCByID(NPC_ID_RAT);

            Location farmhouse = new Location(LOCATION_ID_FARMHOUSE,
            "Farmhouse", "There is a small farmhouse, with a farmer in front.");
            farmhouse.QuestAvailableHere = QuestByID(QUEST_ID_CLEAR_FARMERS_FIELD);

            Location farmersField = new Location(LOCATION_ID_FARM_FIELD,
            "Farmer's field", "You see rows of vegetables growing here.");
            farmersField.NPCAvailableHere = NPCByID(NPC_ID_SNAKE);

            Location guardPost = new Location(LOCATION_ID_GUARD_POST,
            "Guard post", "There is a large, tough-looking guard here.",
            ItemByID(ITEM_ID_ADVENTURER_PASS));
            Location bridge = new Location(LOCATION_ID_BRIDGE,
            "Bridge", "A stone bridge crosses a wide river.");

            Location spiderField = new Location(LOCATION_ID_SPIDER_FIELD,
            "Forest", "You see spider webs covering covering the trees in this forest.");
            spiderField.NPCAvailableHere = NPCByID(NPC_ID_GIANT_SPIDER);

            // Link the locations together
            home.LocationToNorthID = LOCATION_ID_TOWN_SQUARE;

            townSquare.LocationToNorthID = LOCATION_ID_ALCHEMIST_HUT;
            townSquare.LocationToSouthID = LOCATION_ID_HOME;
            townSquare.LocationToEastID = LOCATION_ID_GUARD_POST;
            townSquare.LocationToWestID = LOCATION_ID_FARMHOUSE;

            farmhouse.LocationToEastID = LOCATION_ID_TOWN_SQUARE;
            farmhouse.LocationToWestID = LOCATION_ID_FARM_FIELD;

            farmersField.LocationToEastID = LOCATION_ID_FARMHOUSE;

            alchemistHut.LocationToSouthID = LOCATION_ID_TOWN_SQUARE;
            alchemistHut.LocationToNorthID = LOCATION_ID_ALCHEMISTS_GARDEN;

            alchemistsGarden.LocationToSouthID = LOCATION_ID_ALCHEMIST_HUT;

            guardPost.LocationToEastID = LOCATION_ID_BRIDGE;
            guardPost.LocationToWestID = LOCATION_ID_TOWN_SQUARE;

            bridge.LocationToWestID = LOCATION_ID_GUARD_POST;
            bridge.LocationToEastID = LOCATION_ID_SPIDER_FIELD;

            spiderField.LocationToWestID = LOCATION_ID_BRIDGE;

            // Add the locations to the static list
            Locations.Add(home);
            Locations.Add(townSquare);
            Locations.Add(guardPost);
            Locations.Add(alchemistHut);
            Locations.Add(alchemistsGarden);
            Locations.Add(farmhouse);
            Locations.Add(farmersField);
            Locations.Add(bridge);
            Locations.Add(spiderField);

            foreach ( var location in Locations ) {
                WooTown.AddLocation ( location );
            }
            }


        public static Item ItemByID ( int id ) {
            foreach ( Item item in Items ) {
                if ( item.ID == id ) {
                    return item;
                    }
                }

            return null
           ;
            }

        public static NPC NPCByID ( int id ) {
            foreach ( NPC NPC in NPCs ) {
                if ( NPC.ID == id ) {
                    return NPC;
                    }
                }

            return null;
        }

        public static Enemy EnemyByID ( int id ) {
            foreach ( Enemy enemy in Enemies ) {
                if ( enemy.ID == id ) {
                    return enemy;
                    }
                }

            return null;
            }

        public static Quest QuestByID ( int id ) {
            foreach ( Quest quest in Quests ) {
                if ( quest.ID == id ) {
                    return quest;
                    }
                }

            return null;
        }

        public static Location LocationByID ( int id ) {
            foreach ( Location location in Locations ) {
                if ( location.ID == id ) {
                    return location;
                    }
                }

            return null
           ;
            }
        }

    }