﻿using System;
using System.Collections.Generic;

namespace Engine {
    public class Armor : Item {
        public BonusStats BonusStats;
        public ArmorSlot ArmorSlot;

        public Armor(int id, String name, String namePlural, ArmorSlot armorSlot) : base (id, name, namePlural) {

            }

        public void Equip ( Player wearer ) {
            switch ( ArmorSlot ) {
                case ArmorSlot.Chest:
                    //if the player is wearing a chest armor already, unequip it
                    wearer.ChestArmor?.Unequip ( wearer );
                    wearer.ChestArmor = this;
                    AddBonus ( wearer );
                    break;
                case ArmorSlot.Legs:
                    wearer.LegArmor?.Unequip ( wearer );
                    wearer.LegArmor = this;
                    AddBonus ( wearer );
                    break;
                case ArmorSlot.Head:
                    wearer.HeadArmor?.Unequip(wearer);
                    wearer.HeadArmor = this;
                    AddBonus(wearer);
                    break;
            }
            
        }

        public void AddBonus ( Player wearer ) {
            wearer.Strength += BonusStats.Strength;
            wearer.Dexterity += BonusStats.Dexterity;
            wearer.Intelligence += BonusStats.Intelligence;
            wearer.Endurance += BonusStats.Endurance;
            wearer.AirResistance += BonusStats.AirResistance;
            wearer.EarthResistance += BonusStats.EarthResistance;
            wearer.FireResistance += BonusStats.FireResistance;
            wearer.WaterResistance += BonusStats.WaterResistance;
            wearer.Dodge += BonusStats.Dodge;
            wearer.BlockChance += BonusStats.BlockChance;
            wearer.BonusDamage += BonusStats.BonusDamage;



        }

        public void Unequip ( Player wearer ) {
            wearer.EquippedArmor = null;

        }

        public void RemoveBonus ( Player wearer ) {
            wearer.Strength -= BonusStats.Strength;
            wearer.Dexterity -= BonusStats.Dexterity;
            wearer.Intelligence -= BonusStats.Intelligence;
            wearer.Endurance -= BonusStats.Endurance;
            wearer.AirResistance -= BonusStats.AirResistance;
            wearer.EarthResistance -= BonusStats.EarthResistance;
            wearer.FireResistance -= BonusStats.FireResistance;
            wearer.WaterResistance -= BonusStats.WaterResistance;
            wearer.Dodge -= BonusStats.Dodge;
            wearer.BlockChance -= BonusStats.BlockChance;
            wearer.BonusDamage -= BonusStats.BonusDamage;
            }
        }
    }
