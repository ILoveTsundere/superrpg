﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Engine {
    public class Location {

        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Item ItemRequiredToEnter { get; set; }
        public Quest QuestAvailableHere { get; set; }
        public NPC NPCAvailableHere { get; set; }
        public int? LocationToNorthID { get; set; }
        public int? LocationToEastID { get; set; }
        public int? LocationToSouthID { get; set; }
        public int? LocationToWestID { get; set; }
        public List<Enemy> EnemiesHere { get; set; } 
        public int MonsterEncounterChance { get; set; }

        public bool HasQuest => (QuestAvailableHere != null);

        //Connects this location to another dungeon
        public Dungeon ConnectingDungeon { get; set; }

        //Connects this location to a location in another dungeon.
        public int ConnectingLocationID { get; set; }

        public Location ( int id, string name, string description, Item itemRequired = null, Quest questHere = null, NPC npcHere = null ) {

            ID = id;
            Name = name;
            Description = description;
            EnemiesHere = new List<Enemy> ();

            }

        }
    }
