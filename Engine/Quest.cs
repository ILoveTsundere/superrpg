﻿using System;
using System.Collections.Generic;

namespace Engine {
    public class Quest {

        public int ID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public int RewardExperiencePoints { get; set; }
        public int RewardGold { get; set; }
        public Item RewardItem { get; set; }
        public Item RequiredQuest { get; set; }
        public List<QuestCompletionItem> QuestCompletionItems { get; set; }

        public Quest( int id, String name, String description, int rewardExp, int rewardGold ) {
            ID = id;
            Name = name;
            Description = description;
            RewardExperiencePoints = rewardExp;
            QuestCompletionItems = new List<QuestCompletionItem>();
            RewardGold = rewardGold;
            }

        }
    }
