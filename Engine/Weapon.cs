﻿using System.Collections.Generic;

namespace Engine {
    public class Weapon : Item {
        public int MinimumDamage { get; set; }
        public int MaximumDamage { get; set; }
        public WeaponSlot WeaponSlot;
        public BonusStats BonusStats;
        public List<Ability> BonusAbilities; 


        public Weapon ( int id, string name, string namePlural, int minimumDamage, int maximumDamage, WeaponSlot weaponSlot = WeaponSlot.OneHand, string description = "", int price = 0 ) : base(id, name, namePlural, description, price) {
            MinimumDamage = minimumDamage;
            MaximumDamage = maximumDamage;
            WeaponSlot = weaponSlot;
        }

        public void Equip ( Player wearer ) {

            switch ( WeaponSlot ) {
                case WeaponSlot.OneHand:
                    wearer.Weapon.Unequip ( wearer );
                    wearer.Weapon = this;
                    break;
                case WeaponSlot.Shield:
                    break;
                case WeaponSlot.TwoHand:
                    break;
            }
            
        }

        public void AddBonus ( ) {
            
        }

        public void RemoveBonus ( ) {
            
        }

        public void Unequip ( Player wearer ) {
            
        }
        }
    }
