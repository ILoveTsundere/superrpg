﻿using System;

namespace Engine {
    public class HealingPotion : Potion {

        public int AmountToHeal { get; set; }

        public HealingPotion( int id, String name, String namePlural, int amountToHeal, string description, int price = 0) : base (id, name, namePlural, description, price) {
            AmountToHeal = amountToHeal;
            }

        public new void UseOnTarget ( Entity user, Entity target ) {

            RaiseMessage ( user.Name + " uses " + Name + " on "  );
            target.CurrentHitPoints = (target.CurrentHitPoints + AmountToHeal);



            }
        }
    }
