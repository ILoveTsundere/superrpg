﻿using System;
using System.Collections.Generic;


namespace Engine {
    public class Enemy : Entity {

        public int MaximumDamage { get; set; }
        public int MinimumDamage { get; set; }
        public int RewardExperiencePoints { get; set; }
        public int RewardGold { get; set; }
        public List<LootItem> LootTable { get; set; }
        public EnemyBehaviour Mentality;

        public Enemy ( int id, String name, String description, int minDamage, int maxDamage, int rewardExp, int rewardG, int currHP, int maxHP, int currMP, int maxMP, int level, EnemyBehaviour mentality = EnemyBehaviour.Aggressive, int gold = 0 ) : base (id, name, description, currHP, maxHP, currMP, maxMP, level, gold) {
            MaximumDamage = maxDamage;
            MinimumDamage = minDamage;
            RewardExperiencePoints = rewardExp;
            RewardGold = rewardG;
            LootTable = new List<LootItem>();
            Mentality = mentality;

        }

        protected new int CalculateDamage ( ) {
            return RandomNumberGenerator.RandomDamage ( MaximumDamage - MinimumDamage, MinimumDamage );
        }

        }
    }
