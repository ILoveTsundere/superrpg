﻿using System;

namespace Engine {
    public interface IRaiseMessage {
       void RaiseMessage ( string message, bool addExtraLine);
       event EventHandler<MessageEventArgs> OnMessage;

        }
    }