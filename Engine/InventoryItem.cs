﻿using System.ComponentModel;

namespace Engine {
    public class InventoryItem {
        private Item _details;

        public int ItemID => Details.ID;

        public int Price => Details.Price;

        // description will be plural if over 1 and normal name is 1
        public string Description => Quantity > 1 ? Details.NamePlural : Details.Name;

        public Item Details {
            get { return _details; }
            set {
                _details = value;
            }
        }

        public int Quantity { get; set; }


        public InventoryItem (Item details, int quantity ) {
            Details = details;
            Quantity = quantity;

            }
        
        }
    }
