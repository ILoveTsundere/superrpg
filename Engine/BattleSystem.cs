﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine {
    public class BattleSystem : IRaiseMessage {
        private Player _player;
        private List<Enemy> _enemies;
        public List<Entity> BattleOrder;
        public event EventHandler<MessageEventArgs> OnMessage;

        public BattleSystem ( Player player, List<Enemy> enemies ) {
            _player = player;
            _enemies = new List<Enemy>();
            BattleOrder = new List<Entity>();
            }

        public void AddEnemies ( List<Enemy> enemies ) {
            _enemies = enemies;
            }

        public void Battle ( ) {
            /*Entity nextEntity = BattleOrder[0];
            if ( nextEntity is Enemy ) {
                // if its an enemy, do his logic
                //nextEntity.DoSomething ( );
                }
            else {
                // if its a player character, let the player choose what to do.
                PlayerMove ( );
            }
            BattleOrder.RemoveAt(0);
            //after the attack, reorder entities in stack
            //BattleOrder.OrderBy (  )*/
            }

       public void RaiseMessage ( string message, bool addExtraLine = false ) {

            }


        public void MonsterMentalityAction ( Enemy monster ) {
            switch ( monster.Mentality ) {
                case EnemyBehaviour.Aggressive:
                    break;
            }
        }
        }
    }
