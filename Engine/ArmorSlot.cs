﻿namespace Engine {
    public enum ArmorSlot {
        Chest,
        Hands,
        Legs,
        Feet,
        Head,
        Necklace,
        Ring

    }
}