﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Engine {
    public class Entity : INotifyPropertyChanged, IRaiseMessage {

        #region Fields
        public String Name { get; set; }
        public String Description { get; set; }
        public List<Skill> Skills;
        public List<Spell> Spells;
        private int _currentHP;
        private int _maximumHP;
        private int _currentMP;
        private int _maximumMP;
        private int _gold;
        private int _level;
        public List<StatusModifier> StatusModifiers;
        public event EventHandler<MessageEventArgs> OnMessage;
        public double CritModifier = 2.5;


        private int _physRes;

        public int PhysicalResistance {
            get { return _physRes; }
            set { _physRes = Clamp ( value, 0, 100 ); }
        }


        //nonelemental damage will be considered pure magic

        private int _magicRes;
        public int MagicResistance {
            get { return _magicRes; }
            set { _magicRes = Clamp ( value, 0, 100 ); }
        }

        private int _waterRes;
        public int WaterResistance {
            get { return _waterRes; }
            set {
                _waterRes = Clamp ( value, 0, 200 );
                
            }
        }

        private int _fireRes;
        public int FireResistance {
            get { return _fireRes; }
            set {
                _fireRes = Clamp(value, 0, 200);

                }
            }

        private int _earthRes;
        public int EarthResistance {
            get { return _earthRes; }
            set {
                _earthRes = Clamp(value, 0, 200);

                }
            }

        private int _airRes;
        public int AirResistance {
            get { return _airRes; }
            set {
                _airRes = Clamp(value, 0, 200);

                }
            }

        public int BonusDamage;

        private int _blockChance;
        public int BlockChance {
            get { return _blockChance; }
            set { _blockChance = Clamp ( value, 0, 75 ); }
        }


        public int ID { get; set; }

        public int CurrentHitPoints {
            get { return _currentHP; }
            set {
                _currentHP = value;
                OnPropertyChanged ( "CurrentHitPoints" );
            }
        }

        public int MaximumHitPoints {
            get { return _maximumHP; }
            set {
                _maximumHP = value;
                OnPropertyChanged ( "MaximumHitPoints" );
            }
        }

        public int CurrentManaPoints {
            get { return _currentMP; }
            set {
                _currentMP = value;
                OnPropertyChanged ( "CurrentManaPoints" );
            }
        }

        public int MaximumManaPoints {
            get { return _maximumMP; }
            set {
                _maximumMP = value;
                OnPropertyChanged ( "MaximumManaPoints" );
            }
        }

        public int Gold {
            get { return _gold; }
            set {
                _gold = value;
                OnPropertyChanged ( "Gold" );
            }
        }

        public int Level {
            get { return _level; }
            set {
                _level = value;
                OnPropertyChanged ( "Level" );
            }
        }


        //stats
        private int _str;
        private int _end;
        private int _dex;
        private int _int;
        private int _spd;

        public int Strength {
            get { return _str; }
            set { _str = Clamp ( value, 0, 99 ); }
        }

        public int Dexterity {
            get { return _dex; }
            set { _dex = Clamp ( value, 0, 99 ); }
        }

        public int Endurance {
            get { return _end; }
            set { _end = Clamp ( value, 0, 99 ); }
        }

        public int Intelligence {
            get { return _int; }
            set { _int = Clamp ( value, 0, 99 ); }
        }

        public int Speed {
            set { _spd = value; }
            // speed will be speed + 1/4 of dexterity
            //TODO : make it math ceiling
            get { return _spd + (int) ( Dexterity * ( 0.25 ) ); }
        }

        //chance to hit, dodge and critical will be calculated by a mix of equipment, stats, skills and weapon.
        public int ChanceToHit { get; set; }
        public int Dodge { get; set; }
        public int Critical { get; set; }

        //equipped armor and weapon
        public Armor EquippedArmor { get; set; }
        public Weapon EquippedWeapon { get; set; }

        #endregion

        public Entity (int id, String name, String description, int currentHP, int maxHP, int currMP, int maxMP, int level, int gold = 0 ) {

            ID = id;
            this.Name = name;
            this.Description = description;
            this.Gold = gold;
            this.Level = level;
            this.CurrentHitPoints = currentHP;
            this.MaximumHitPoints = maxHP;
            this.CurrentManaPoints = currMP;
            this.MaximumManaPoints = maxMP;
            Skills = new List<Skill> ();
            ChanceToHit = 200;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged ( string name ) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
            }

        public int Clamp (int value, int minValue, int maxValue ) {
            if ( value > maxValue ) {
                return maxValue;
            }
            else if ( value < minValue ) {
                return minValue;
            }

            return value;

        }

        protected int CalculateDamage ( ) {
            return RandomNumberGenerator.RandomDamage ( 3, 2 );
        }

        protected bool CalculateHit ( Entity target ) {

            var hitChance = ChanceToHit - target.Dodge;
            return ( RandomNumberGenerator.CalculateDropChanceFrom0to99 ( ) <= hitChance );

        }

        protected bool CalculateCrit ( Entity target ) {
            var critChance = target.Critical;
            return ( RandomNumberGenerator.CalculateDropChanceFrom0to99 ( ) <= critChance );
        }

        public void Attack ( Entity target ) {
            RaiseMessage ( Name + " attacks " + target.Name );
            //calculate the chance of attack hitting
            if ( CalculateHit ( target ) ) {
                var damageDone = CalculateDamage ( );
                if ( CalculateCrit ( target ) ) {
                    damageDone = (int)(damageDone * CritModifier);
                    //crit, for now its a 2.5 damage modifier, might get skills that modify it (probably passive ones)
                    RaiseMessage( target.Name + " has been crit for " + damageDone * CritModifier + "!!!");

                }
                //attack hit
                RaiseMessage ( target.Name + " has been hit for " + damageDone );

                target.CurrentHitPoints -= damageDone;
            }
            else {
                RaiseMessage ( Name + " misses his attack on " + target.Name );
            }

        }

        public void UseAbility ( Ability ability, Entity target ) {
            //TODO: Consider putting ability specific messages by having them on the skill itself
            RaiseMessage ( Name + " used " + ability.Name + " on " + target.Name );
            //check if the spell hit
            if ( RandomNumberGenerator.CalculateDropChanceFrom0to99 ( ) <= ability.ChanceToHit ) {
                //if the spell is meant to do damage
                if ( ability.Damage <= 0 ) {
                    var damageDone = 0;
                    switch ( ability.ElementalAttribute ) {
                        case ElementalAttribute.Air:
                            damageDone = ability.Damage - (ability.Damage * (AirResistance/100) );
                            
                            break;
                        case ElementalAttribute.Earth:
                            damageDone = ability.Damage - (ability.Damage * (EarthResistance / 100));

                            break;
                        case ElementalAttribute.Water:
                            damageDone = ability.Damage - (ability.Damage * (WaterResistance / 100));

                            break;
                        case ElementalAttribute.Fire:
                            damageDone = ability.Damage - (ability.Damage * (FireResistance / 100));


                            break;
                        case ElementalAttribute.NonElemental:
                            damageDone = ability.Damage ;

                            break;

                    }

                    target._currentHP -= damageDone;
                    RaiseMessage(target.Name + " has been hit for " + damageDone + " " + ability.ElementalAttribute + " damage.");

                    }
                
                target.StatusModifiers.Add(ability.StatusModifer);
                RaiseMessage ( target.Name + " : " + ability.StatusModifer.Description );



            }
            else {
                RaiseMessage ( ability.Name + " has missed! " );
            }
        }
        


        public void RaiseMessage ( string message, bool addExtraLine = false ) {
            OnMessage?.Invoke(this, new MessageEventArgs(message, addExtraLine));
            }

        }
    }
