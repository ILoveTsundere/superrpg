﻿using System;

namespace Engine {
    public class PC : Entity {

        public PC ( int id, String name, String description, int currHP, int maxHP, int currMP, int maxMP, int exp, int gold,
            int level ) : base(id, name, description, currHP, maxHP, currMP, maxMP, level, gold) {
            }
        }
    }
