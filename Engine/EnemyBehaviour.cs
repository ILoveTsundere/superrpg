﻿namespace Engine {
    public enum EnemyBehaviour {
        Aggressive,
        Passive,
        Healer,
        SpellCaster,
        Buffer,
        Debuffer
    }
}