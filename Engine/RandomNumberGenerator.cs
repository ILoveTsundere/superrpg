﻿using PCLCrypto;

namespace Engine {
    public static class RandomNumberGenerator {

        public static uint CalculateDropChanceFrom0to99 () {
            return WinRTCrypto.CryptographicBuffer.GenerateRandomNumber() % 100;
            }

        public static int RandomDamage ( int maxMinus, int minimumValue ) {
            return (int)(WinRTCrypto.CryptographicBuffer.GenerateRandomNumber() % (maxMinus + 1)) + minimumValue;
            }

        }
    }
