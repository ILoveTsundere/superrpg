﻿namespace Engine {
    public enum AbilityType {
        Debuff,
        Buff,
        Damage
    }
}