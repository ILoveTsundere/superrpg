﻿namespace Engine {
    public enum ElementalAttribute {
        Fire,
        Water,
        Air,
        Earth,
        NonElemental
    }
}