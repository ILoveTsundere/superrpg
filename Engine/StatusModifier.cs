﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Engine {
    public class StatusModifier {

        private const int PermanentModifier = -1;
        
        public int ID;
        public string Name;
        public int Duration;
        public bool IsRepeatable;
        public Operations Operation;
        public bool IsActive = false;
        //what it does
        public string Description;


        public StatusModifier ( int id, string name, int duration, bool isRepeatable, Operations operation ) {

            ID = id;
            Name = name;
            Duration = duration;
            IsRepeatable = isRepeatable;
            Operation = operation;

        }

        //activate the status modifier, this will called upon being added
        public void Activate ( ) {
            IsActive = true;
        }

        //lowers duration by 1 and does whatever the status modifier is meant to do.
        public void TickDown ( ) {
            if ( !IsActive ) return;
            //if the duration is permanent
            if ( IsRepeatable ) {

                switch ( Operation ) {
                    case Operations.Addition:
                        break;
                    case Operations.Division:
                        break;
                    case Operations.Multiplication:
                        break;
                    case Operations.Substraction:
                        break;
                    }

                }

            // if its a permanent modifier, skip lowering the reduction and checking for active.
            if ( Duration == PermanentModifier )
                return;

            Duration -= 1;
            if ( Duration <= 0 ) {
                IsActive = false;

                }


            }

        }
    }
