﻿using Newtonsoft.Json;

namespace Engine {
    public class Ability {
        public Targetable Targetable;
        public PossibleTargets PosTargets;
        public string Name;
        public int ChanceToHit;
        public int Damage;
        public StatusModifier StatusModifer;
        public DamageAttribute DamageAttribute;
        public ElementalAttribute ElementalAttribute;
        public int ManaCost;

    }
    }
