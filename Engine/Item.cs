﻿using System;

namespace Engine {
    public class Item : IRaiseMessage {

        public int ID { get; set; }
        public String Name { get; set; }
        public String NamePlural { get; set; }
        public int Price { get; set; }
        public event EventHandler<MessageEventArgs> OnMessage;

        //required level for the player to use that item
        public int LevelRequiredToUse { get; set; }

        public Item( int id, String name, String namePlural, string description = "", int price = 0, int levelRequired = 0 ) {
            ID = id;
            Name = name;
            NamePlural = namePlural;
            LevelRequiredToUse = levelRequired;
            Price = price;
        }

        public void RaiseMessage ( string message, bool addExtraLine = false) {
            OnMessage?.Invoke(this, new MessageEventArgs(message, addExtraLine));
            }


        }
        

    }
