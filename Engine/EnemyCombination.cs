﻿using System.Collections.Generic;

namespace Engine {
    public struct EnemyCombination {
        public List<Enemy> EnemyCombo;
        public int EncounterChance;
        public int EscapeDifficulty;
    }
}
