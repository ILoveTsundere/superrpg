﻿namespace Engine {
    public enum DebuffType {
        Poison,
        Freeze,
        MinusElementalRes,
        MinusPhysicalRes,
        MinusMagicalRes,
        MinusStat, 
        Stun,
        Burn
    }
}