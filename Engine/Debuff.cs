﻿namespace Engine {
    public class Debuff : StatusModifier {
        public DebuffType DebuffType;

        public Debuff ( int id, string name, int duration, bool isRepeatable, Operations operation, DebuffType debuffType ) : base ( id, name, duration, isRepeatable, operation ) {
            DebuffType = debuffType;
        }
    }
}