﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Engine {
    public class NPC : Entity, INotifyPropertyChanged {

        //Every NPC will have an inventory and possible trade stuff with the player.
        public List<InventoryItem> Inventory { get; set; }
        public new event PropertyChangedEventHandler PropertyChanged;

        public NPC ( int id, String name, String description, int currHP, int maxHP, int currMP, int maxMP, int level, int gold = 0 ) : base (id, name, description, currHP, maxHP, currMP, maxMP, level, gold) {
            Inventory = new List<InventoryItem>();

            }

        public void AddItemToInventory ( Item itemToAdd, int quantity = 1 ) {
            InventoryItem item = Inventory.SingleOrDefault(ii => ii.Details.ID == itemToAdd.ID);


            if ( item == null )
                Inventory.Add(new InventoryItem(itemToAdd, quantity));
            else
                item.Quantity += quantity;

            OnPropertyChanged("NPCInventory");

            }

        public void RemoveItemFromInventory ( Item itemToRemove, int quantity = 1 ) {
            InventoryItem item = Inventory.SingleOrDefault(ii => ii.Details.ID == itemToRemove.ID);

            if ( item == null )
                return;

            item.Quantity -= quantity;

            //if item quantity is 0 or lower, remove it.
            if ( item.Quantity <= 0 )
                Inventory.Remove(item);

            OnPropertyChanged("NPCInventory");
            }

        protected new void OnPropertyChanged ( string name ) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
            }
        }

    }
