﻿namespace Engine {
    public enum PossibleTargets {
        Single, Multiple, BackRow, FrontRow, Line
    }
}