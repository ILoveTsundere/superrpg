﻿namespace Engine {
    public class Buff : StatusModifier {
        public Buff ( int id, string name, int duration, bool isRepeatable, Operations operation ) : base(id, name, duration, isRepeatable, operation) {
            }
        }
    }