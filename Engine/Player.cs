﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Engine {
    public class Player : Entity {
        private Dungeon _currentDungeon;
        private Location _currentLocation;
        private Enemy _currentMonster;
        public uint AttributePoints;

        public Armor ChestArmor;
        public Armor HeadArmor;
        public Armor LegArmor;

        public Weapon Weapon;
        public Weapon OffHand;


        public Player ( int id, String name, String description, int currHP, int maxHP, int currMP, int maxMP, int exp, int gold, int level ) : base ( id, name, description, currHP, maxHP, currMP, maxMP, level, gold ) {
            ExperiencePoints = exp;
            Inventory = new List<InventoryItem> ( );
            ExperiencePointsForNextLevel = 20;
            Quests = new List<PlayerQuest> ( );
            EquippedArmor = null;
            EquippedWeapon = null;
            AttributePoints = 5;
        }

        public List<PlayerQuest> Quests { get; set; }
        public List<InventoryItem> Inventory { get; set; }
        //private BattleSystem _battleSystem;

        public Dungeon CurrentDungeon {
            get { return _currentDungeon; }
            set {
                _currentDungeon = value;
                OnPropertyChanged ( "Dungeon" );
            }
        }

        public Location CurrentLocation {
            get { return _currentLocation; }
            set {
                _currentLocation = value;
                OnPropertyChanged ( "CurrentLocation" );
            }
        }

        public List<PC> Party { get; set; }

        public List<Weapon> Weapons {
            get { return Inventory.Where ( x => x.Details is Weapon ).Select ( x => x.Details as Weapon ).ToList ( ); }
        }

        public List<Potion> Potions {
            get { return Inventory.Where ( x => x.Details is Potion ).Select ( x => x.Details as Potion ).ToList ( ); }
        }

        public int ExperiencePointsForNextLevel { get; set; }
        public int ExperiencePoints { get; set; }

        public void AddItemToInventory ( Item itemToAdd, int quantity = 1 ) {
            InventoryItem item = Inventory.SingleOrDefault ( ii => ii.Details.ID == itemToAdd.ID );
            if ( item == null )
                // They didn't have the item, so add it to their inventory
                Inventory.Add ( new InventoryItem ( itemToAdd, quantity ) );
            else
            // They have the item in their inventory, so increase the quantity
                item.Quantity += quantity;

            RaiseInventoryChangedEvent ( itemToAdd );
        }

        public bool CompletedQuest ( Quest quest ) {
            return ( from playerQuest in Quests where playerQuest.Details.ID == quest.ID select playerQuest.IsCompleted ).FirstOrDefault ( );
        }

        public void FromJSONString ( string json ) {
            var loadPlayer = JsonConvert.DeserializeObject<Player> ( json );

            //location & dungeon
            CurrentLocation = loadPlayer.CurrentLocation;
            CurrentDungeon = loadPlayer.CurrentDungeon;

            //equipped items
            EquippedWeapon = loadPlayer.EquippedWeapon;

            //details
            Name = loadPlayer.Name;
            Description = loadPlayer.Description;

            //experience
            Level = loadPlayer.Level;
            ExperiencePoints = loadPlayer.ExperiencePoints;
            ExperiencePointsForNextLevel = loadPlayer.ExperiencePointsForNextLevel;

            //loading hp and mp
            CurrentHitPoints = loadPlayer.CurrentHitPoints;
            MaximumHitPoints = loadPlayer.MaximumHitPoints;
            CurrentManaPoints = loadPlayer.CurrentManaPoints;
            MaximumManaPoints = loadPlayer.MaximumManaPoints;

            //gold
            Gold = loadPlayer.Gold;

            //stats
            Strength = loadPlayer.Strength;
            Endurance = loadPlayer.Endurance;
            Dexterity = loadPlayer.Dexterity;
            Intelligence = loadPlayer.Intelligence;
            ChanceToHit = loadPlayer.ChanceToHit;
            Dodge = loadPlayer.Dodge;
            Critical = loadPlayer.Critical;
            BlockChance = loadPlayer.BlockChance;
            CritModifier = loadPlayer.CritModifier;
            AirResistance = loadPlayer.AirResistance;
            WaterResistance = loadPlayer.WaterResistance;
            EarthResistance = loadPlayer.EarthResistance;
            FireResistance = loadPlayer.FireResistance;
            MagicResistance = loadPlayer.MagicResistance;
            PhysicalResistance = loadPlayer.PhysicalResistance;

            //Quests
            Quests = loadPlayer.Quests;

            //Inventory
            Inventory = loadPlayer.Inventory;

            //Party (WIP)
            Party = loadPlayer.Party;
        }

        public void GainExperience ( int xp ) {
            //checks if the added xp surpasses the xp needed for next level
            if ( xp + ExperiencePoints > ExperiencePointsForNextLevel ) {
                //leveled up
                LevelUp ( );

                var leftoverExp = xp + ExperiencePoints - ExperiencePointsForNextLevel;
                ExperiencePointsForNextLevel = (int) ( ExperiencePointsForNextLevel + ExperiencePointsForNextLevel * 0.1 );
                ExperiencePoints = leftoverExp;
            }
            else {
                //if not leveled up, adds xp
                ExperiencePoints += xp;
            }
        }

        private void GivePlayerQuestRewards ( Quest quest ) {
            RaiseMessage ( "" );
            RaiseMessage ( "You complete the " + quest.Name + " quest." );

            // Remove quest items from inventory
            RemoveQuestCompletionItems ( quest );

            RaiseMessage ( "You receive: " );
            RaiseMessage ( quest.RewardExperiencePoints + " experience points" );
            RaiseMessage ( quest.RewardGold + " gold" );
            RaiseMessage ( quest.RewardItem.Name, true );

            GainExperience ( quest.RewardExperiencePoints );
            Gold += quest.RewardGold;

            //add reward item to player inventory
            AddItemToInventory ( quest.RewardItem );

            //mark quest completed
            MarkQuestCompleted ( quest );
        }

        private void GiveQuestToPlayer ( Quest quest ) {
            // Display the messages
            RaiseMessage ( "You receive the " + quest.Name + " quest.", true );
            RaiseMessage ( quest.Description );
            RaiseMessage ( "To complete it, return with:" );
            foreach ( QuestCompletionItem qci in quest.QuestCompletionItems ) {
                if ( qci.Quantity == 1 )
                    RaiseMessage ( qci.Quantity + " " + qci.Details.Name, true );
                else
                    RaiseMessage ( qci.Quantity + " " + qci.Details.NamePlural, true );
            }
            // Add the quest to the player's quest list
            Quests.Add ( new PlayerQuest ( quest ) );
            RaiseQuestChangeEvent ( );
        }

        public bool HasAllQuestCompletionItems ( Quest quest ) {
            //go through all the quest completion items required for this quest
            return quest.QuestCompletionItems.All ( qci => Inventory.Exists ( ii => ii.Details.ID == qci.Details.ID && ii.Quantity >= qci.Quantity ) );
            //if he does, return true
        }

        public bool HasQuest ( Quest quest ) {
            return Quests.Exists ( pq => pq.Details.ID == quest.ID );
        }

        public bool HasRequiredItemToEnterLocation ( Location location ) {
            return location.ItemRequiredToEnter == null || Inventory.Exists ( ii => ii.Details.ID == location.ItemRequiredToEnter.ID );
        }

        private void LevelUp ( ) {
            Level += 1;
            MaximumHitPoints += 10;
        }

        public void MarkQuestCompleted ( Quest quest ) {
            // Find the quest in the player's quest list
            PlayerQuest playerQuest = Quests.SingleOrDefault ( pq => pq.Details.ID == quest.ID );

            // if the quest is found, complete it
            if ( playerQuest != null )
                playerQuest.IsCompleted = true;

            RaiseQuestChangeEvent ( );
        }

        public void MonsterAttack ( ) {
            //Monster still alive, 5 - 3, 3
            var damageToPlayer = (int) RandomNumberGenerator.RandomDamage ( _currentMonster.MaximumDamage - _currentMonster.MinimumDamage, _currentMonster.MinimumDamage );


            //display damage on mainScreen
            RaiseMessage ( "The " + _currentMonster.Name + " did " + damageToPlayer + " points of damage." );

            CurrentHitPoints -= damageToPlayer;


            if ( CurrentHitPoints <= 0 ) {
                RaiseMessage ( "The " + _currentMonster.Name + " killed you." );

                MoveTo ( World.LOCATION_ID_HOME );
            }
        }

        public void MoveEast ( ) {
            if ( CurrentLocation.LocationToEastID == null ) return;
            RaiseMessage ( "You move east." );
            MoveTo ( CurrentLocation.LocationToEastID );
        }

        public void MoveNorth ( ) {
            if ( CurrentLocation.LocationToNorthID == null ) return;
            RaiseMessage ( "You move north." );
            MoveTo ( CurrentLocation.LocationToNorthID );
        }

        public void MoveSouth ( ) {
            if ( CurrentLocation.LocationToSouthID == null ) return;
            RaiseMessage ( "You move south." );
            MoveTo ( CurrentLocation.LocationToSouthID );
        }

        public void MoveTo ( int? newLocationID ) {
            Location newLocation = _currentDungeon.LocationByID ( newLocationID );

            _currentLocation = newLocation;

            // Does the location have any required items
            if ( !HasRequiredItemToEnterLocation ( newLocation ) ) {
                RaiseMessage ( "You must have a " + newLocation.ItemRequiredToEnter.Name + " to enter this location." );
                return;
            }

            CurrentLocation = newLocation;

            if ( newLocation.HasQuest ) {
                if ( HasQuest ( newLocation.QuestAvailableHere ) ) {
                    if ( !CompletedQuest ( newLocation.QuestAvailableHere ) && HasAllQuestCompletionItems ( newLocation.QuestAvailableHere ) ) {
                        //if the player has not completed it and has all the items to complete it
                        GivePlayerQuestRewards ( newLocation.QuestAvailableHere );
                    }
                }
                else {
                    GiveQuestToPlayer ( newLocation.QuestAvailableHere );
                }
                RaiseMessage ( "" );
            }

            // Does the location have an NPC?
            if ( newLocation.NPCAvailableHere != null ) {

            }

            if ( newLocation.EnemiesHere.Count > 0 ) {
                RaiseMessage("You see a " + newLocation.NPCAvailableHere.Name);

                // Make a new monster, using the values from the standard monster in the World.Monster list
                Enemy standardMonster = World.EnemyByID(newLocation.EnemiesHere[0].ID);

                _currentMonster = new Enemy(standardMonster.ID, standardMonster.Name, standardMonster.Description,
                    standardMonster.MinimumDamage, standardMonster.MaximumDamage, standardMonster.RewardExperiencePoints,
                    standardMonster.RewardGold, standardMonster.CurrentHitPoints, standardMonster.MaximumHitPoints,
                    standardMonster.CurrentManaPoints, standardMonster.MaximumManaPoints, standardMonster.Level, standardMonster.Mentality,
                    standardMonster.Gold);

                foreach ( LootItem lootItem in standardMonster.LootTable )
                    _currentMonster.LootTable.Add(lootItem);
                }
            }

        public void MoveWest ( ) {
            if ( CurrentLocation.LocationToWestID == null ) return;
            RaiseMessage ( "You move west." );
            MoveTo ( CurrentLocation.LocationToWestID );
        }


        private void RaiseInventoryChangedEvent ( Item item ) {
            //raises change in inveotry
            OnPropertyChanged ( "Inventory" );
            if ( item is Weapon )
                OnPropertyChanged ( "Weapons" );
            if ( item is Potion )
                OnPropertyChanged ( "Potions" );
        }


        private void RaiseQuestChangeEvent ( ) {
            OnPropertyChanged ( "Quest" );
        }

        //Removes an item from player's inventory.
        public void RemoveItem ( Item item ) {
            foreach ( InventoryItem ii in Inventory.Where ( ii => ii.Details.ID == item.ID ) ) {
                ii.Quantity--;
                break;
            }
        }

        public void RemoveItemFromInventory ( Item itemToRemove, int quantity = 1 ) {
            InventoryItem item = Inventory.SingleOrDefault ( ii => ii.Details.ID == itemToRemove.ID );
            if ( item == null ) {
                // The item is not in the player's inventory, so ignore it.
                // We might want to raise an error for this situation
            }
            else {
                // They have the item in their inventory,so decrease the quantityra
                item.Quantity -= quantity;

                // Don't allow negative quantities. We might want to raise an error for this situation
                if ( item.Quantity < 0 )
                    item.Quantity = 0;

                // If the quantity is zero, remove the item from the list
                if ( item.Quantity == 0 )
                    Inventory.Remove ( item );

                // Notify the UI that the inventory has changed
                RaiseInventoryChangedEvent ( itemToRemove );
            }
        }

        public void RemoveQuestCompletionItems ( Quest quest ) {
            foreach ( QuestCompletionItem qci in
                quest.QuestCompletionItems ) {
                // Subtract the quantity from the player's inventory that was needed to complete the quest
                InventoryItem item = Inventory.SingleOrDefault ( ii => ii.Details.ID == qci.Details.ID );
                if ( item != null ) RemoveItemFromInventory ( item.Details, qci.Quantity );
            }
        }

        public string ToJSONString ( ) {
            JsonConvert.DefaultSettings = ( ) => new JsonSerializerSettings {
                Formatting = Formatting.Indented,
                TypeNameHandling = TypeNameHandling.Objects,
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize
            };
            return JsonConvert.SerializeObject ( this, Formatting.Indented );
        }

        public void UsePotion ( Potion potion ) {
            potion.UseOnTarget ( this, this );


            RemoveItemFromInventory ( potion );


        }

        public void UseWeapon ( ) {
            //get the currently selected weapon from the combobox
            //Weapon currentWeapon = (Weapon)weaponsCBO.SelectedItem;
            int damageToMonster;
            if ( EquippedWeapon != null ) {
                //determine the amount of damage to do to the monster
                damageToMonster = (int) RandomNumberGenerator.RandomDamage ( EquippedWeapon.MaximumDamage - EquippedWeapon.MinimumDamage, EquippedWeapon.MinimumDamage );
            }
            else damageToMonster = 1;
            //apply damage
            _currentMonster.CurrentHitPoints -= damageToMonster;

            RaiseMessage ( "You hit the " + _currentMonster.Name + " for " + damageToMonster + " points." );

            if ( _currentMonster.CurrentHitPoints <= 0 ) {
                RaiseMessage ( Environment.NewLine + "You defeated the " + _currentMonster.Name );
                RaiseMessage ( "You receive " + _currentMonster.RewardExperiencePoints + " experience points" );
                RaiseMessage ( "You receive " + _currentMonster.RewardGold + " gold", true );

                Gold += _currentMonster.RewardGold;
                GainExperience ( _currentMonster.RewardExperiencePoints );

                //loot list from monster
                var lootedItems = new List<InventoryItem> ( );

                foreach ( LootItem lootItem in _currentMonster.LootTable ) if ( RandomNumberGenerator.CalculateDropChanceFrom0to99 ( ) <= lootItem.DropPercentage ) lootedItems.Add ( new InventoryItem ( lootItem.Details, 1 ) );

                // If no items were randomly selected, then add the default loot item(s).
                if ( lootedItems.Count == 0 ) foreach ( LootItem lootItem in _currentMonster.LootTable ) if ( lootItem.IsDefaultItem ) lootedItems.Add ( new InventoryItem ( lootItem.Details, 1 ) );

                foreach ( InventoryItem inventoryItem in lootedItems ) {
                    AddItemToInventory ( inventoryItem.Details );

                    if ( inventoryItem.Quantity == 1 ) RaiseMessage ( "You loot " + inventoryItem.Quantity + " " + inventoryItem.Details.Name );
                    else RaiseMessage ( "You loot " + inventoryItem.Quantity + " " + inventoryItem.Details.NamePlural );
                }

                RaiseMessage ( "" );
                MoveTo ( CurrentLocation.ID );
            }

            else MonsterAttack ( );
        }
        }
} ;