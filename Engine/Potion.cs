﻿using System;
using System.Collections.Generic;

namespace Engine {
   public class Potion : Item {

       public List<StatusModifier> StatusModifier;

        public Potion (int id, String name, String namePlural, string description = "", int price = 0 ) : base (id, name, namePlural, description, price) {
            StatusModifier = new List<StatusModifier> ();
            }

        public virtual void UseOnTarget (Entity user, Entity target ) {
            

            }

       public void AddStatusModifier ( StatusModifier statusModifier )  {
           StatusModifier.Add ( statusModifier );
       }
        }
    }
