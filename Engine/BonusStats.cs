﻿using System;
using System.Collections;

namespace Engine {
    public struct BonusStats {

        public int WaterResistance;
        public int FireResistance;
        public int EarthResistance;
        public int AirResistance;
        public int Strength;
        public int Endurance;
        public int Dexterity;
        public int Intelligence;
        public int BlockChance;
        public int BonusDamage;
        public int Dodge;


    }
}