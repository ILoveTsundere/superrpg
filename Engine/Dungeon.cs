﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices.ComTypes;

namespace Engine {
    public class Dungeon { 
        //Keeps track of the list of locations, this is so they can be saved.
        public List<Location> dungeonMap;
        public Location CurrentLocation { get; set; }
        public List<EnemyCombination> EnemiesHere;
        public int BattleChance; 

        //will keep track of each npc to save their current inventory (and maybe if they're not alive anymore?)
        public List<NPC> DungeonNPCs; 
        public string Name { get; set; }
        public int ID { get; set; }

        public Dungeon ( int id, string name ) {
            dungeonMap = new List<Location> ();
            DungeonNPCs = new List<NPC> ();
            Name = name;
            ID = id;
        }

        public void AddLocation ( Location location ) {
            dungeonMap.Add(location);
            }
        

        public Location LocationByID ( int? id ) {

            return dungeonMap.Single ( l => l.ID == id );

        }

        }
    }
