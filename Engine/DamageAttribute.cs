﻿namespace Engine {
    public enum DamageAttribute {
        Strength,
        Agility,
        Intelligence
    }
}