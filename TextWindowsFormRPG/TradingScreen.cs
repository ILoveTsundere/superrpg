﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Engine;

namespace TextWindowsFormRPG {
    public partial class TradingScreen : Form {

        private readonly Player _player;
        private readonly NPC _vendor;

        public TradingScreen ( Player player ) {
            InitializeComponent();
            _player = player;
            _vendor = _player.CurrentLocation.NPCAvailableHere;

            vendorItemsDGV.CellClick += vendorItemsDGV_CellClick;
            playerItemsDGV.CellClick += playerItemsDGV_CellClick;

            var rightAlignedCellStyle = new DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleRight };

            playerItemsDGV.RowHeadersVisible = false;
            playerItemsDGV.AutoGenerateColumns = false;

            playerItemsDGV.ColumnCount = 3;

            playerItemsDGV.Columns[0].Name = "Name";
            playerItemsDGV.Columns[1].Name = "Quantity";
            playerItemsDGV.Columns[2].Name = "Price";

            playerItemsDGV.Columns.Add(new DataGridViewTextBoxColumn {
                DataPropertyName = "ItemID",
                Visible = false
                });

            playerItemsDGV.Columns.Add(new DataGridViewButtonColumn {
                Text = "Sell 1",
                UseColumnTextForButtonValue = true,
                DataPropertyName = "ItemID"
                });

            //VENDOR
            vendorItemsDGV.RowHeadersVisible = false;
            vendorItemsDGV.AutoGenerateColumns = false;


            vendorItemsDGV.ColumnCount = 3;

            vendorItemsDGV.Columns[0].Name = "Name";
            vendorItemsDGV.Columns[1].Name = "Quantity";
            vendorItemsDGV.Columns[2].Name = "Price";

            vendorItemsDGV.Columns.Add(new DataGridViewTextBoxColumn {
                DataPropertyName = "ItemID",
                Visible = false
                });


            vendorItemsDGV.Columns.Add(new DataGridViewButtonColumn {
                Text = "Buy 1",
                UseColumnTextForButtonValue = true,
                DataPropertyName = "ItemID"
                });

            gRemainLabel.Text = _player.Gold.ToString();
            vendorGoldLabel.Text = _vendor.Gold.ToString();

            UpdateTradeDGV();

            }

        private void UpdateTradeDGV ( ) {

            
            // Refresh player's inventory list
            playerItemsDGV.Rows.Clear();
            foreach ( var inventoryItem in _player.Inventory )
                playerItemsDGV.Rows.Add(new object[] { inventoryItem.Details.Name, inventoryItem.Quantity.ToString(), inventoryItem.Price.ToString(), inventoryItem.ItemID.ToString() });


            // Refresh vendor's inventory list
            vendorItemsDGV.Rows.Clear();
            foreach ( var inventoryItem in _vendor.Inventory )
                vendorItemsDGV.Rows.Add(new object[] { inventoryItem.Details.Name, inventoryItem.Quantity.ToString(), inventoryItem.Price.ToString(), inventoryItem.ItemID.ToString() });


            }

        private void closeButton_Click ( object sender, EventArgs e ) {
            Close();
            }

        private void playerItemsDGV_CellClick ( object sender, DataGridViewCellEventArgs e ) {

            if ( e.ColumnIndex == 4 ) {
                object itemID = playerItemsDGV.Rows[e.RowIndex].Cells[3].Value;
                var price = Convert.ToInt32(playerItemsDGV.Rows[e.RowIndex].Cells[2].Value);

                Item itemBeingSold = World.ItemByID(Convert.ToInt32(itemID));

                if ( itemBeingSold.ID == World.UNSELLABLE_ITEM_ID ) {
                    MessageBox.Show ( "You cannot sell this item." );
                }
                else if ( _vendor.Gold < itemBeingSold.Price ) {
                    MessageBox.Show ( "I don't have enough money to buy that from you." );
                }
                else {
                    _player.RemoveItemFromInventory ( itemBeingSold, 1 );
                    _player.Gold += price;
                    _vendor.AddItemToInventory ( itemBeingSold, 1 );
                    _vendor.Gold -= price;
                    gRemainLabel.Text = _player.Gold.ToString ( );
                    vendorGoldLabel.Text = _vendor.Gold.ToString ( );
                    UpdateTradeDGV ( );

                }

            }

            }

        private void vendorItemsDGV_CellClick ( object sender, DataGridViewCellEventArgs e ) {

            if ( e.ColumnIndex == 4 ) {
                var itemID = vendorItemsDGV.Rows[e.RowIndex].Cells[3].Value;
                var price = Convert.ToInt32(playerItemsDGV.Rows[e.RowIndex].Cells[2].Value);

                Item itemBeingSold = World.ItemByID(Convert.ToInt32(itemID));


                if ( _player.Gold < itemBeingSold.Price ) {
                    MessageBox.Show("You don't have the gold to buy this.");

                    }
                else {
                    _player.Gold -= price;
                    _vendor.Gold += price;
                    gRemainLabel.Text = _player.Gold.ToString ( );
                    vendorGoldLabel.Text = _vendor.Gold.ToString();
                    _vendor.RemoveItemFromInventory ( itemBeingSold, 1 );
                    _player.AddItemToInventory ( itemBeingSold, 1 );
                    UpdateTradeDGV ( );
                }
            }

            }
        }
    }
