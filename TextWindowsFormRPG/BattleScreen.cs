﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Engine;

namespace TextWindowsFormRPG {


    public partial class BattleScreen : Form {
        private Player _player;
        private Entity _monster;

        public BattleScreen ( Player player, Enemy monster ) {
            InitializeComponent();
            _player = player;
            _monster = monster;
            _monster.OnMessage += DisplayMessage;
            _player.OnMessage += DisplayMessage;
            monsHPLbl.Text = _monster.CurrentHitPoints + "/" + _monster.MaximumHitPoints;
            playerHPLbl.Text = _player.CurrentHitPoints + "/" + _player.MaximumHitPoints;

            _player.PropertyChanged += OnPropertyChanged;
            _monster.PropertyChanged += OnPropertyChanged;
            

        }

        private void DisplayMessage ( object sender, MessageEventArgs messageEventArgs ) {
            mainRTB.Text += messageEventArgs.Message + Environment.NewLine;
            if ( messageEventArgs.AddExtraNewLine )
                mainRTB.Text += Environment.NewLine;

            mainRTB.SelectionStart = mainRTB.Text.Length;
            mainRTB.ScrollToCaret();

            }

        private void OnPropertyChanged ( object sender, PropertyChangedEventArgs e ) {
            if ( sender is Enemy ) {
                if ( e.PropertyName == "CurrentHitPoints" ) {
                    monsHPLbl.Text = _monster.CurrentHitPoints.ToString() + "/" + _monster.MaximumHitPoints;

                    }

                }
            else {
                if ( e.PropertyName == "CurrentHitPoints" ) {
                    playerHPLbl.Text = _player.CurrentHitPoints.ToString ( ) + "/" + _player.MaximumHitPoints;
                }
                }

                }

        private void fightButton_Click ( object sender, EventArgs e ) {
            switch ( fightButton.Text ) {
                case "Fight":
                    fightButton.Text = "Attack";
                    break;
                case "Attack":
                    _player.Attack(_monster);
                    _monster.Attack(_player);
                    break;
            }
            }

        private void itemsButton_Click ( object sender, EventArgs e ) {
            switch ( itemsButton.Text ) {
                case "Items":
                    ShowItems ( );
                    break;
            }

            }

        private void escapeButton_Click ( object sender, EventArgs e ) {
            //try to escape, waste turn if fails
            
            }

        private void ShowItems ( ) {
            foreach ( InventoryItem ii in _player.Inventory ) {
                
            }
            }

        private void backButton_Click ( object sender, EventArgs e ) {

            }
        }
        }
    
