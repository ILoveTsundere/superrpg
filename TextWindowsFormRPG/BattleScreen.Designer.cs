﻿namespace TextWindowsFormRPG {
    partial class BattleScreen {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose ( bool disposing ) {
            if ( disposing && (components != null) ) {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ( ) {
            this.mainRTB = new System.Windows.Forms.RichTextBox();
            this.playerGB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.playerHPLbl = new System.Windows.Forms.Label();
            this.playerMPLbl = new System.Windows.Forms.Label();
            this.monsGB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.monsHPLbl = new System.Windows.Forms.Label();
            this.monsMPLbl = new System.Windows.Forms.Label();
            this.fightButton = new System.Windows.Forms.Button();
            this.itemsButton = new System.Windows.Forms.Button();
            this.escapeButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.playerGB.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.monsGB.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainRTB
            // 
            this.mainRTB.Location = new System.Drawing.Point(168, 13);
            this.mainRTB.Name = "mainRTB";
            this.mainRTB.Size = new System.Drawing.Size(548, 440);
            this.mainRTB.TabIndex = 0;
            this.mainRTB.Text = "";
            // 
            // playerGB
            // 
            this.playerGB.Controls.Add(this.tableLayoutPanel1);
            this.playerGB.Location = new System.Drawing.Point(12, 13);
            this.playerGB.Name = "playerGB";
            this.playerGB.Size = new System.Drawing.Size(150, 70);
            this.playerGB.TabIndex = 3;
            this.playerGB.TabStop = false;
            this.playerGB.Text = "Player";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.playerHPLbl, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.playerMPLbl, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(144, 51);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "HP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "MP";
            // 
            // playerHPLbl
            // 
            this.playerHPLbl.AutoSize = true;
            this.playerHPLbl.Location = new System.Drawing.Point(50, 0);
            this.playerHPLbl.Name = "playerHPLbl";
            this.playerHPLbl.Size = new System.Drawing.Size(36, 13);
            this.playerHPLbl.TabIndex = 2;
            this.playerHPLbl.Text = "11/20";
            // 
            // playerMPLbl
            // 
            this.playerMPLbl.AutoSize = true;
            this.playerMPLbl.Location = new System.Drawing.Point(50, 25);
            this.playerMPLbl.Name = "playerMPLbl";
            this.playerMPLbl.Size = new System.Drawing.Size(36, 13);
            this.playerMPLbl.TabIndex = 3;
            this.playerMPLbl.Text = "20/20";
            // 
            // monsGB
            // 
            this.monsGB.Controls.Add(this.tableLayoutPanel3);
            this.monsGB.Location = new System.Drawing.Point(722, 13);
            this.monsGB.Name = "monsGB";
            this.monsGB.Size = new System.Drawing.Size(150, 70);
            this.monsGB.TabIndex = 6;
            this.monsGB.TabStop = false;
            this.monsGB.Text = "Monster";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.63441F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.36559F));
            this.tableLayoutPanel3.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.monsHPLbl, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.monsMPLbl, 1, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(144, 51);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "HP";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "MP";
            // 
            // monsHPLbl
            // 
            this.monsHPLbl.AutoSize = true;
            this.monsHPLbl.Location = new System.Drawing.Point(57, 0);
            this.monsHPLbl.Name = "monsHPLbl";
            this.monsHPLbl.Size = new System.Drawing.Size(36, 13);
            this.monsHPLbl.TabIndex = 2;
            this.monsHPLbl.Text = "11/20";
            // 
            // monsMPLbl
            // 
            this.monsMPLbl.AutoSize = true;
            this.monsMPLbl.Location = new System.Drawing.Point(57, 25);
            this.monsMPLbl.Name = "monsMPLbl";
            this.monsMPLbl.Size = new System.Drawing.Size(36, 13);
            this.monsMPLbl.TabIndex = 3;
            this.monsMPLbl.Text = "20/20";
            // 
            // fightButton
            // 
            this.fightButton.Location = new System.Drawing.Point(12, 481);
            this.fightButton.Name = "fightButton";
            this.fightButton.Size = new System.Drawing.Size(75, 23);
            this.fightButton.TabIndex = 7;
            this.fightButton.Text = "Fight";
            this.fightButton.UseVisualStyleBackColor = true;
            this.fightButton.Click += new System.EventHandler(this.fightButton_Click);
            // 
            // itemsButton
            // 
            this.itemsButton.Location = new System.Drawing.Point(93, 480);
            this.itemsButton.Name = "itemsButton";
            this.itemsButton.Size = new System.Drawing.Size(75, 23);
            this.itemsButton.TabIndex = 8;
            this.itemsButton.Text = "Items";
            this.itemsButton.UseVisualStyleBackColor = true;
            this.itemsButton.Click += new System.EventHandler(this.itemsButton_Click);
            // 
            // escapeButton
            // 
            this.escapeButton.Location = new System.Drawing.Point(12, 527);
            this.escapeButton.Name = "escapeButton";
            this.escapeButton.Size = new System.Drawing.Size(75, 23);
            this.escapeButton.TabIndex = 9;
            this.escapeButton.Text = "Escape";
            this.escapeButton.UseVisualStyleBackColor = true;
            this.escapeButton.Click += new System.EventHandler(this.escapeButton_Click);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(93, 527);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 10;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // BattleScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 562);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.escapeButton);
            this.Controls.Add(this.itemsButton);
            this.Controls.Add(this.fightButton);
            this.Controls.Add(this.monsGB);
            this.Controls.Add(this.playerGB);
            this.Controls.Add(this.mainRTB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "BattleScreen";
            this.Text = "BattleScreen";
            this.playerGB.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.monsGB.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

            }

        #endregion
        private System.Windows.Forms.RichTextBox mainRTB;
        private System.Windows.Forms.GroupBox playerGB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label playerHPLbl;
        private System.Windows.Forms.Label playerMPLbl;
        private System.Windows.Forms.GroupBox monsGB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label monsHPLbl;
        private System.Windows.Forms.Label monsMPLbl;
        private System.Windows.Forms.Button fightButton;
        private System.Windows.Forms.Button itemsButton;
        private System.Windows.Forms.Button escapeButton;
        private System.Windows.Forms.Button backButton;
        }
    }