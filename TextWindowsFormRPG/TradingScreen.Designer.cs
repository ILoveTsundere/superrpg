﻿namespace TextWindowsFormRPG {
    partial class TradingScreen {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose ( bool disposing ) {
            if ( disposing && (components != null) ) {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ( ) {
            this.vendorItemsDGV = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.closeButton = new System.Windows.Forms.Button();
            this.playerItemsDGV = new System.Windows.Forms.DataGridView();
            this.gRemainLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.vendorGoldLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.vendorItemsDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerItemsDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // vendorItemsDGV
            // 
            this.vendorItemsDGV.AllowUserToAddRows = false;
            this.vendorItemsDGV.AllowUserToDeleteRows = false;
            this.vendorItemsDGV.AllowUserToOrderColumns = true;
            this.vendorItemsDGV.AllowUserToResizeRows = false;
            this.vendorItemsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vendorItemsDGV.Location = new System.Drawing.Point(452, 45);
            this.vendorItemsDGV.MultiSelect = false;
            this.vendorItemsDGV.Name = "vendorItemsDGV";
            this.vendorItemsDGV.ReadOnly = true;
            this.vendorItemsDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.vendorItemsDGV.Size = new System.Drawing.Size(420, 450);
            this.vendorItemsDGV.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(186, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "My Inventory:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(619, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Vendor\'s Inventory:";
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(406, 501);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 4;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // playerItemsDGV
            // 
            this.playerItemsDGV.AllowUserToAddRows = false;
            this.playerItemsDGV.AllowUserToDeleteRows = false;
            this.playerItemsDGV.AllowUserToOrderColumns = true;
            this.playerItemsDGV.AllowUserToResizeRows = false;
            this.playerItemsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.playerItemsDGV.Location = new System.Drawing.Point(12, 45);
            this.playerItemsDGV.MultiSelect = false;
            this.playerItemsDGV.Name = "playerItemsDGV";
            this.playerItemsDGV.ReadOnly = true;
            this.playerItemsDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.playerItemsDGV.Size = new System.Drawing.Size(420, 450);
            this.playerItemsDGV.TabIndex = 5;
            // 
            // gRemainLabel
            // 
            this.gRemainLabel.AutoSize = true;
            this.gRemainLabel.Location = new System.Drawing.Point(50, 29);
            this.gRemainLabel.Name = "gRemainLabel";
            this.gRemainLabel.Size = new System.Drawing.Size(35, 13);
            this.gRemainLabel.TabIndex = 6;
            this.gRemainLabel.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Gold:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(452, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Vendor Gold:";
            // 
            // vendorGoldLabel
            // 
            this.vendorGoldLabel.AutoSize = true;
            this.vendorGoldLabel.Location = new System.Drawing.Point(528, 29);
            this.vendorGoldLabel.Name = "vendorGoldLabel";
            this.vendorGoldLabel.Size = new System.Drawing.Size(16, 13);
            this.vendorGoldLabel.TabIndex = 9;
            this.vendorGoldLabel.Text = "er";
            // 
            // TradingScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 562);
            this.Controls.Add(this.vendorGoldLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gRemainLabel);
            this.Controls.Add(this.playerItemsDGV);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.vendorItemsDGV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TradingScreen";
            this.Text = "TradingScreen";
            ((System.ComponentModel.ISupportInitialize)(this.vendorItemsDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerItemsDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

            }

        #endregion
        private System.Windows.Forms.DataGridView vendorItemsDGV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.DataGridView playerItemsDGV;
        private System.Windows.Forms.Label gRemainLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label vendorGoldLabel;
        }
    }