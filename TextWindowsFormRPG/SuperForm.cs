﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Engine;

namespace TextWindowsFormRPG {
    public partial class SuperForm : Form {

        private readonly Player _player;
        private Enemy _enemy;
        private string _jsonSave;

        public SuperForm ( ) {
            InitializeComponent();
            
            _player = new Player(0, "Test", "ME", 100, 100, 5, 5, 0, 20, 1);
            _player.CurrentDungeon = World.WooTown;

            _enemy = World.EnemyByID ( World.NPC_ID_GIANT_SPIDER );

            hpLabel.DataBindings.Add("Text", _player, "CurrentHitPoints");
            mpLabel.DataBindings.Add("Text", _player, "CurrentManaPoints");
            lvLabel.DataBindings.Add("Text", _player, "Level");
            xpLabel.DataBindings.Add("Text", _player, "ExperiencePoints");
            gLabel.DataBindings.Add("Text", _player, "Gold");

            _player.OnMessage += DisplayMessage;
            _enemy.OnMessage += DisplayMessage;

            _player.AddItemToInventory(new Weapon(World.ITEM_ID_RUSTY_SWORD, "Rusty sword", "Rusty swords", 50, 0, WeaponSlot.OneHand, "Rusty", 5), 1);


            levelUpButton.Visible = false;
            _player.PropertyChanged += PlayerOnPropertyChanged;

            _player.MoveTo(_player.CurrentDungeon.LocationByID(World.LOCATION_ID_HOME).ID);

            }


        private void DisplayMessage ( object sender, MessageEventArgs messageEventArgs ) {
            mainRTB.Text += messageEventArgs.Message + Environment.NewLine;
            if ( messageEventArgs.AddExtraNewLine )
                mainRTB.Text += Environment.NewLine;

            mainRTB.SelectionStart = mainRTB.Text.Length;
            mainRTB.ScrollToCaret();

            }

        private void PlayerOnPropertyChanged ( object sender, PropertyChangedEventArgs e ) {

            switch ( e.PropertyName ) {
                case "NPCInventory":
                    UpdateInventoryListInUi();
                    break;

                case "Quest":
                    UpdateQuestListInUi();
                    break;

                case "Inventory":
                    UpdateInventoryListInUi();
                    break;

                case "Level":
                    var player = sender as Player;
                    if ( player != null )
                        player.AttributePoints += 5;
                    levelUpButton.Visible = true;
                    break;

                case "CurrentLocation":
                    northButton.Visible = (_player.CurrentLocation.LocationToNorthID != null);
                    eastButton.Visible = (_player.CurrentLocation.LocationToEastID != null);
                    southButton.Visible = (_player.CurrentLocation.LocationToSouthID != null);
                    westButton.Visible = (_player.CurrentLocation.LocationToWestID != null);

                    locationRTB.Text = _player.CurrentLocation.Name + Environment.NewLine;
                    locationRTB.Text += _player.CurrentLocation.Description + Environment.NewLine;
                    if ( _player.CurrentLocation.NPCAvailableHere != null ) {
                        mainRTB.Text += "NPC HERE\n";
                    }
                    if ( _player.CurrentLocation.EnemiesHere.Count > 0 ) {
                        searchButton.Visible = true;
                    }

                    break;
                }
            }

        private void SuperForm_Load ( object sender, EventArgs e ) {
            }

        private void northButton_Click ( object sender, EventArgs e ) {
            _player.MoveNorth();

            }

        private void eastButton_Click ( object sender, EventArgs e ) {
            _player.MoveEast();

            }

        private void westButton_Click ( object sender, EventArgs e ) {
            _player.MoveWest();

            }

        private void southButton_Click ( object sender, EventArgs e ) {
            _player.MoveSouth();

            }

        private void MoveTo ( Location newLocation ) {

            UpdateInventoryListInUi();
            UpdateQuestListInUi();


            }


        private void UpdateInventoryListInUi ( ) {
            // Refresh player's inventory list
            invDGV.RowHeadersVisible = false;

            invDGV.ColumnCount = 2;
            invDGV.Columns[0].Name = "Name";
            invDGV.Columns[1].Name = "Quantity";

            invDGV.Rows.Clear();

            foreach ( InventoryItem inventoryItem in _player.Inventory.Where(inventoryItem => inventoryItem.Quantity > 0) ) {
                invDGV.Rows.Add(new object[] { inventoryItem.Details.Name, inventoryItem.Quantity.ToString() });
                }

            }



        private void UpdateQuestListInUi ( ) {
            // Refresh player's quest list
            questDGV.RowHeadersVisible = false;

            questDGV.ColumnCount = 2;
            questDGV.Columns[0].Name = "Name";
            questDGV.Columns[0].Width = 197;
            questDGV.Columns[1].Name = "Done?";

            questDGV.Rows.Clear();

            foreach ( PlayerQuest playerQuest in _player.Quests ) {
                questDGV.Rows.Add(new[] { playerQuest.Details.Name, playerQuest.IsCompleted.ToString() });
                }

            }

        private void useWepButton_Click ( object sender, EventArgs e ) {

            _player.UseWeapon();


            }

        private void ScrollToBottomOfMessages ( ) {
            mainRTB.SelectionStart = mainRTB.Text.Length;
            mainRTB.ScrollToCaret();
            }



        //Quiet load back into the position the player was in already.
        private void LoadPlayerLocation ( Location newLocation ) {
            _player.CurrentLocation = newLocation;
            northButton.Visible = (newLocation.LocationToNorthID != null);
            eastButton.Visible = (newLocation.LocationToEastID != null);
            southButton.Visible = (newLocation.LocationToSouthID != null);
            westButton.Visible = (newLocation.LocationToWestID != null);

            locationRTB.Text = newLocation.Name + Environment.NewLine;
            locationRTB.Text += newLocation.Description + Environment.NewLine;
            }

        private void saveButton_Click ( object sender, EventArgs e ) {
            _jsonSave = _player.ToJSONString();
            mainRTB.Text = _jsonSave;
            string folderName = "C:/Users/Carlos2/Desktop/test.txt";
            System.IO.StreamWriter file = new System.IO.StreamWriter(folderName);
            file.WriteLine(_jsonSave);

            file.Close();

            }

        private void loadButton_Click ( object sender, EventArgs e ) {

            string folderName = "C:/Users/Carlos2/Desktop/test.txt";
            _jsonSave = File.ReadAllText(folderName);

            _player.FromJSONString(_jsonSave);
            LoadPlayerLocation(_player.CurrentLocation);
            UpdateInventoryListInUi();
            UpdateQuestListInUi();

            }


        private void tradeButton_Click ( object sender, EventArgs e ) {
            TradingScreen tradingScreen = new TradingScreen(_player);
            tradingScreen.StartPosition = FormStartPosition.CenterParent;
            tradingScreen.ShowDialog(this);
            }

        private void button1_Click ( object sender, EventArgs e ) {
            BattleScreen battle = new BattleScreen (_player, _enemy);
            battle.StartPosition = FormStartPosition.CenterParent;
            battle.ShowDialog(this);

            }

        private void mainMenuButton_Click ( object sender, EventArgs e ) {

            }
        }
    }
