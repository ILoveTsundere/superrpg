﻿namespace TextWindowsFormRPG {
    partial class SuperForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose ( bool disposing ) {
            if ( disposing && (components != null) ) {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ( ) {
            this.statusButton = new System.Windows.Forms.Button();
            this.equipmentButton = new System.Windows.Forms.Button();
            this.northButton = new System.Windows.Forms.Button();
            this.westButton = new System.Windows.Forms.Button();
            this.southButton = new System.Windows.Forms.Button();
            this.eastButton = new System.Windows.Forms.Button();
            this.mainRTB = new System.Windows.Forms.RichTextBox();
            this.mainMenuButton = new System.Windows.Forms.Button();
            this.locationRTB = new System.Windows.Forms.RichTextBox();
            this.manaLabel = new System.Windows.Forms.Label();
            this.lvLabel = new System.Windows.Forms.Label();
            this.levelLabel = new System.Windows.Forms.Label();
            this.hpLabel = new System.Windows.Forms.Label();
            this.gLabel = new System.Windows.Forms.Label();
            this.mpLabel = new System.Windows.Forms.Label();
            this.xpLabel = new System.Windows.Forms.Label();
            this.goldLabel = new System.Windows.Forms.Label();
            this.experienceLabel = new System.Windows.Forms.Label();
            this.hitPointsLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.locLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.locationLabel = new System.Windows.Forms.Label();
            this.invDGV = new System.Windows.Forms.DataGridView();
            this.questDGV = new System.Windows.Forms.DataGridView();
            this.saveButton = new System.Windows.Forms.Button();
            this.loadButton = new System.Windows.Forms.Button();
            this.tradeButton = new System.Windows.Forms.Button();
            this.searchButton = new System.Windows.Forms.Button();
            this.levelUpButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.invDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.questDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // statusButton
            // 
            this.statusButton.Location = new System.Drawing.Point(693, 452);
            this.statusButton.Name = "statusButton";
            this.statusButton.Size = new System.Drawing.Size(75, 23);
            this.statusButton.TabIndex = 7;
            this.statusButton.Text = "Status";
            this.statusButton.UseVisualStyleBackColor = true;
            // 
            // equipmentButton
            // 
            this.equipmentButton.Location = new System.Drawing.Point(774, 452);
            this.equipmentButton.Name = "equipmentButton";
            this.equipmentButton.Size = new System.Drawing.Size(75, 23);
            this.equipmentButton.TabIndex = 9;
            this.equipmentButton.Text = "Equipment";
            this.equipmentButton.UseVisualStyleBackColor = true;
            // 
            // northButton
            // 
            this.northButton.Location = new System.Drawing.Point(360, 450);
            this.northButton.Name = "northButton";
            this.northButton.Size = new System.Drawing.Size(75, 23);
            this.northButton.TabIndex = 14;
            this.northButton.Text = "North";
            this.northButton.UseVisualStyleBackColor = true;
            this.northButton.Click += new System.EventHandler(this.northButton_Click);
            // 
            // westButton
            // 
            this.westButton.Location = new System.Drawing.Point(314, 479);
            this.westButton.Name = "westButton";
            this.westButton.Size = new System.Drawing.Size(75, 23);
            this.westButton.TabIndex = 15;
            this.westButton.Text = "West";
            this.westButton.UseVisualStyleBackColor = true;
            this.westButton.Click += new System.EventHandler(this.westButton_Click);
            // 
            // southButton
            // 
            this.southButton.Location = new System.Drawing.Point(360, 508);
            this.southButton.Name = "southButton";
            this.southButton.Size = new System.Drawing.Size(75, 23);
            this.southButton.TabIndex = 16;
            this.southButton.Text = "South";
            this.southButton.UseVisualStyleBackColor = true;
            this.southButton.Click += new System.EventHandler(this.southButton_Click);
            // 
            // eastButton
            // 
            this.eastButton.Location = new System.Drawing.Point(404, 479);
            this.eastButton.Name = "eastButton";
            this.eastButton.Size = new System.Drawing.Size(75, 23);
            this.eastButton.TabIndex = 17;
            this.eastButton.Text = "East";
            this.eastButton.UseVisualStyleBackColor = true;
            this.eastButton.Click += new System.EventHandler(this.eastButton_Click);
            // 
            // mainRTB
            // 
            this.mainRTB.Location = new System.Drawing.Point(200, 6);
            this.mainRTB.Name = "mainRTB";
            this.mainRTB.Size = new System.Drawing.Size(424, 432);
            this.mainRTB.TabIndex = 21;
            this.mainRTB.Text = "";
            // 
            // mainMenuButton
            // 
            this.mainMenuButton.Location = new System.Drawing.Point(38, 450);
            this.mainMenuButton.Name = "mainMenuButton";
            this.mainMenuButton.Size = new System.Drawing.Size(75, 23);
            this.mainMenuButton.TabIndex = 22;
            this.mainMenuButton.Text = "Main Menu";
            this.mainMenuButton.UseVisualStyleBackColor = true;
            this.mainMenuButton.Click += new System.EventHandler(this.mainMenuButton_Click);
            // 
            // locationRTB
            // 
            this.locationRTB.Location = new System.Drawing.Point(632, 6);
            this.locationRTB.Name = "locationRTB";
            this.locationRTB.Size = new System.Drawing.Size(240, 201);
            this.locationRTB.TabIndex = 23;
            this.locationRTB.Text = "";
            // 
            // manaLabel
            // 
            this.manaLabel.AutoSize = true;
            this.manaLabel.Location = new System.Drawing.Point(3, 58);
            this.manaLabel.Name = "manaLabel";
            this.manaLabel.Size = new System.Drawing.Size(69, 13);
            this.manaLabel.TabIndex = 25;
            this.manaLabel.Text = "Mana Points:";
            // 
            // lvLabel
            // 
            this.lvLabel.AutoSize = true;
            this.lvLabel.Location = new System.Drawing.Point(98, 98);
            this.lvLabel.Name = "lvLabel";
            this.lvLabel.Size = new System.Drawing.Size(35, 13);
            this.lvLabel.TabIndex = 33;
            this.lvLabel.Text = "label7";
            // 
            // levelLabel
            // 
            this.levelLabel.AutoSize = true;
            this.levelLabel.Location = new System.Drawing.Point(3, 98);
            this.levelLabel.Name = "levelLabel";
            this.levelLabel.Size = new System.Drawing.Size(36, 13);
            this.levelLabel.TabIndex = 32;
            this.levelLabel.Text = "Level:";
            // 
            // hpLabel
            // 
            this.hpLabel.AutoSize = true;
            this.hpLabel.Location = new System.Drawing.Point(98, 29);
            this.hpLabel.Name = "hpLabel";
            this.hpLabel.Size = new System.Drawing.Size(35, 13);
            this.hpLabel.TabIndex = 31;
            this.hpLabel.Text = "label1";
            // 
            // gLabel
            // 
            this.gLabel.AutoSize = true;
            this.gLabel.Location = new System.Drawing.Point(98, 78);
            this.gLabel.Name = "gLabel";
            this.gLabel.Size = new System.Drawing.Size(35, 13);
            this.gLabel.TabIndex = 30;
            this.gLabel.Text = "label7";
            // 
            // mpLabel
            // 
            this.mpLabel.AutoSize = true;
            this.mpLabel.Location = new System.Drawing.Point(98, 58);
            this.mpLabel.Name = "mpLabel";
            this.mpLabel.Size = new System.Drawing.Size(35, 13);
            this.mpLabel.TabIndex = 29;
            this.mpLabel.Text = "label6";
            // 
            // xpLabel
            // 
            this.xpLabel.AutoSize = true;
            this.xpLabel.Location = new System.Drawing.Point(98, 118);
            this.xpLabel.Name = "xpLabel";
            this.xpLabel.Size = new System.Drawing.Size(35, 13);
            this.xpLabel.TabIndex = 28;
            this.xpLabel.Text = "label5";
            // 
            // goldLabel
            // 
            this.goldLabel.AutoSize = true;
            this.goldLabel.Location = new System.Drawing.Point(3, 78);
            this.goldLabel.Name = "goldLabel";
            this.goldLabel.Size = new System.Drawing.Size(32, 13);
            this.goldLabel.TabIndex = 27;
            this.goldLabel.Text = "Gold:";
            // 
            // experienceLabel
            // 
            this.experienceLabel.AutoSize = true;
            this.experienceLabel.Location = new System.Drawing.Point(3, 118);
            this.experienceLabel.Name = "experienceLabel";
            this.experienceLabel.Size = new System.Drawing.Size(63, 13);
            this.experienceLabel.TabIndex = 26;
            this.experienceLabel.Text = "Experience:";
            // 
            // hitPointsLabel
            // 
            this.hitPointsLabel.AutoSize = true;
            this.hitPointsLabel.Location = new System.Drawing.Point(3, 29);
            this.hitPointsLabel.Name = "hitPointsLabel";
            this.hitPointsLabel.Size = new System.Drawing.Size(55, 13);
            this.hitPointsLabel.TabIndex = 24;
            this.hitPointsLabel.Text = "Hit Points:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.locLabel, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.nameLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.locationLabel, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.experienceLabel, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.xpLabel, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lvLabel, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.levelLabel, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.goldLabel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.gLabel, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.manaLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.mpLabel, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.hitPointsLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.hpLabel, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(191, 158);
            this.tableLayoutPanel1.TabIndex = 34;
            // 
            // locLabel
            // 
            this.locLabel.AutoSize = true;
            this.locLabel.Location = new System.Drawing.Point(98, 138);
            this.locLabel.Name = "locLabel";
            this.locLabel.Size = new System.Drawing.Size(63, 13);
            this.locLabel.TabIndex = 38;
            this.locLabel.Text = "WoooTown";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(3, 0);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(49, 13);
            this.nameLabel.TabIndex = 36;
            this.nameLabel.Text = "MyName";
            // 
            // locationLabel
            // 
            this.locationLabel.AutoSize = true;
            this.locationLabel.Location = new System.Drawing.Point(3, 138);
            this.locationLabel.Name = "locationLabel";
            this.locationLabel.Size = new System.Drawing.Size(51, 13);
            this.locationLabel.TabIndex = 37;
            this.locationLabel.Text = "Location:";
            // 
            // invDGV
            // 
            this.invDGV.AllowUserToAddRows = false;
            this.invDGV.AllowUserToDeleteRows = false;
            this.invDGV.AllowUserToResizeRows = false;
            this.invDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.invDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.invDGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.invDGV.Enabled = false;
            this.invDGV.Location = new System.Drawing.Point(9, 171);
            this.invDGV.MultiSelect = false;
            this.invDGV.Name = "invDGV";
            this.invDGV.ReadOnly = true;
            this.invDGV.RowHeadersVisible = false;
            this.invDGV.Size = new System.Drawing.Size(185, 267);
            this.invDGV.TabIndex = 35;
            // 
            // questDGV
            // 
            this.questDGV.AllowUserToAddRows = false;
            this.questDGV.AllowUserToDeleteRows = false;
            this.questDGV.AllowUserToResizeRows = false;
            this.questDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.questDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.questDGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.questDGV.Enabled = false;
            this.questDGV.Location = new System.Drawing.Point(632, 213);
            this.questDGV.MultiSelect = false;
            this.questDGV.Name = "questDGV";
            this.questDGV.ReadOnly = true;
            this.questDGV.RowHeadersVisible = false;
            this.questDGV.Size = new System.Drawing.Size(240, 225);
            this.questDGV.TabIndex = 36;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(38, 481);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 41;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point(38, 510);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(75, 23);
            this.loadButton.TabIndex = 42;
            this.loadButton.Text = "Load";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // tradeButton
            // 
            this.tradeButton.Location = new System.Drawing.Point(775, 483);
            this.tradeButton.Name = "tradeButton";
            this.tradeButton.Size = new System.Drawing.Size(75, 23);
            this.tradeButton.TabIndex = 43;
            this.tradeButton.Text = "Trade";
            this.tradeButton.UseVisualStyleBackColor = true;
            this.tradeButton.Click += new System.EventHandler(this.tradeButton_Click);
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(694, 483);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 44;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            // 
            // levelUpButton
            // 
            this.levelUpButton.Location = new System.Drawing.Point(694, 512);
            this.levelUpButton.Name = "levelUpButton";
            this.levelUpButton.Size = new System.Drawing.Size(75, 23);
            this.levelUpButton.TabIndex = 45;
            this.levelUpButton.Text = "Level Up";
            this.levelUpButton.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(775, 512);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 46;
            this.button1.Text = "test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SuperForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 562);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.levelUpButton);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.tradeButton);
            this.Controls.Add(this.loadButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.questDGV);
            this.Controls.Add(this.invDGV);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.locationRTB);
            this.Controls.Add(this.mainMenuButton);
            this.Controls.Add(this.mainRTB);
            this.Controls.Add(this.eastButton);
            this.Controls.Add(this.southButton);
            this.Controls.Add(this.westButton);
            this.Controls.Add(this.northButton);
            this.Controls.Add(this.equipmentButton);
            this.Controls.Add(this.statusButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "SuperForm";
            this.Text = "TextRPG";
            this.Load += new System.EventHandler(this.SuperForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.invDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.questDGV)).EndInit();
            this.ResumeLayout(false);

            }

        #endregion
        private System.Windows.Forms.Button statusButton;
        private System.Windows.Forms.Button equipmentButton;
        private System.Windows.Forms.Button northButton;
        private System.Windows.Forms.Button westButton;
        private System.Windows.Forms.Button southButton;
        private System.Windows.Forms.Button eastButton;
        private System.Windows.Forms.RichTextBox mainRTB;
        private System.Windows.Forms.Button mainMenuButton;
        private System.Windows.Forms.RichTextBox locationRTB;
        private System.Windows.Forms.Label manaLabel;
        private System.Windows.Forms.Label lvLabel;
        private System.Windows.Forms.Label levelLabel;
        private System.Windows.Forms.Label hpLabel;
        private System.Windows.Forms.Label gLabel;
        private System.Windows.Forms.Label mpLabel;
        private System.Windows.Forms.Label xpLabel;
        private System.Windows.Forms.Label goldLabel;
        private System.Windows.Forms.Label experienceLabel;
        private System.Windows.Forms.Label hitPointsLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label locLabel;
        private System.Windows.Forms.Label locationLabel;
        private System.Windows.Forms.DataGridView invDGV;
        private System.Windows.Forms.DataGridView questDGV;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.Button tradeButton;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button levelUpButton;
        private System.Windows.Forms.Button button1;
        }
    }

