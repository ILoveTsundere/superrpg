﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Engine;

namespace SuperRPGConsole {
    public class Program {

        private const string PLAYER_DATA = "C:/Users/Carlos2/Desktop/test.txt";
        private static Player _player;

        static void Main ( string[] args ) {

            LoadGameData();
            Console.WriteLine("Type 'Help' or '?' to see list of commands");
            Console.WriteLine("");

            DisplayCurrentLocation();

            _player.PropertyChanged += Player_OnPropertyChanged;
            _player.OnMessage += Player_OnMessage;

            while ( true ) {

                Console.Write(">");

                string userInput = Console.ReadLine();

                if ( string.IsNullOrWhiteSpace(userInput) ) {
                    continue;
                    }

                string cleanedInput = userInput.ToLower();

                if ( cleanedInput == "exit" ) {
                    SaveGameData();
                    break;
                    }

                ParseInput(cleanedInput);

                }
            }

        private static void Player_OnMessage ( object sender, MessageEventArgs e ) {
            Console.WriteLine(e.Message);
            if ( e.AddExtraNewLine ) {
                Console.WriteLine("");
                }
            }

        private static void Player_OnPropertyChanged ( object sender, PropertyChangedEventArgs e ) {
            if ( e.PropertyName == "CurrentLocation" ) {
                DisplayCurrentLocation();
                if ( _player.CurrentLocation.NPCAvailableHere is Vendor ) {
                    Console.WriteLine("You see a vendor here: {0}", _player.CurrentLocation.NPCAvailableHere.Name);
                    }
                }
            }

        private static void ParseInput ( string input ) {


            if ( input.StartsWith("attack") ) {
                if ( input == "attack" )
                    Console.WriteLine("Attack what?");
                }

            else if ( input.StartsWith("equip") ) {
                if ( input == "equp" )
                    Console.WriteLine("Equp what?");
                }

            else if ( input.StartsWith("drink") ) {
                if ( input == "drink" )
                    Console.WriteLine("drink what?");
                }

            else if ( input.StartsWith("buy") ) {
                if ( input == "buy" ) {
                    Console.WriteLine ( "Buy what?" );
                    return;
                    }

                
            }

            else if ( input.Contains("sell") ) {
                if ( input == "sell" )
                    Console.WriteLine("Sell what?");

                }
            else if ( input.StartsWith ( "use" ) ) {
                if ( input == "use" )
                    Console.WriteLine("Use what?");
                }
            else {


                switch ( input ) {

                    case "help":
                    case "?":
                        Console.WriteLine("Available commands");
                        Console.WriteLine("====================================");
                        Console.WriteLine("Stats - Display player information");
                        Console.WriteLine("Use - Use an item");
                        Console.WriteLine("Look - Get the description of your location");
                        Console.WriteLine("Inventory - Display your inventory");
                        Console.WriteLine("Quests - Display your quests");
                        Console.WriteLine("Attack - Fight the monster");
                        Console.WriteLine("Equip <weapon name> - Set your current weapon");
                        Console.WriteLine("Drink <potion name> - Drink a potion");
                        Console.WriteLine("Trade - display your inventory and vendor's inventory");
                        Console.WriteLine("Buy <item name> - Buy an item from a vendor");
                        Console.WriteLine("Sell <item name> - Sell an item to a vendor");
                        Console.WriteLine("North - Move North");
                        Console.WriteLine("South - Move South");
                        Console.WriteLine("East - Move East");
                        Console.WriteLine("West - Move West");
                        Console.WriteLine("Exit - Save the game and exit");
                        break;

                    case "stats":
                        Console.WriteLine("Name: {0}", _player.Name);
                        Console.WriteLine("HP: {0}/{1}", _player.CurrentHitPoints, _player.MaximumHitPoints);
                        Console.WriteLine("MP: {0}/{1}", _player.CurrentManaPoints, _player.MaximumManaPoints);
                        Console.WriteLine("Level: {0}", _player.Level);
                        Console.WriteLine("Exp. for next level: {0}", _player.ExperiencePointsForNextLevel);
                        break;

                    case "look":
                        DisplayCurrentLocation();

                        break;

                    case "inventory":
                        foreach ( InventoryItem inventoryItem in _player.Inventory )
                            Console.WriteLine("{0}:{1}", inventoryItem.Description, inventoryItem.Quantity);
                        break;

                    case "quests":
                        foreach ( PlayerQuest pq in _player.Quests )
                            Console.WriteLine("{0}:{1} Completed?:{2}", pq.Details.Name, pq.Details.Description, pq.IsCompleted);
                        break;

                    case "trade":
                        if ( _player.CurrentLocation.NPCAvailableHere == null ) {
                            Console.WriteLine ( "Nobody to trade with here." );
                            break;
                        }
                        var vendor = _player.CurrentLocation.NPCAvailableHere as Vendor;
                        if ( vendor != null ) {
                            Console.WriteLine ( "VENDOR INVENTORY" );
                            Console.WriteLine("======================");
                            if ( vendor.Inventory.Count == 0 ) {
                                Console.WriteLine("The Vendor doesn't have anything to sell.");
                            }
                            else {
                                foreach (InventoryItem ii in vendor.Inventory )
                                    Console.WriteLine("{0} {1} Price {2}", ii.Quantity, ii.Description, ii.Price);
                            }
                        }
                        else {
                            Console.WriteLine("You cannot trade with this NPC.");
                        }

                        break;

                    case "north":
                    case "move north":
                        if (_player.CurrentLocation.LocationToNorth == null)
                            Console.WriteLine("You cannot move north");
                        else 
                            _player.MoveNorth (  );
                        break;

                    case "east":
                    case "move east":
                        if ( _player.CurrentLocation.LocationToEast == null )
                            Console.WriteLine("You cannot move east");
                        else
                            _player.MoveEast();
                        break;

                    case "south":
                    case "move south":
                        if ( _player.CurrentLocation.LocationToSouth == null )
                            Console.WriteLine("You cannot move south");
                        else
                            _player.MoveSouth();
                        break;

                    case "west":
                    case "move west":
                        if ( _player.CurrentLocation.LocationToWest == null )
                            Console.WriteLine("You cannot move west");
                        else
                            _player.MoveWest();
                        break;

                    default:
                        Console.WriteLine("Unknown command, type 'Help' for full list of commands");
                        break;

                    }
                }
            }

        private static void SaveGameData ( ) {
            }

        private static void DisplayCurrentLocation ( ) {
            }

        private static void LoadGameData ( ) {
            _player = new Player("Test", "ME", 100, 100, 5, 5, 0, 20, 1);
            _player.CurrentLocation = World.LocationByID ( World.LOCATION_ID_HOME );
        }
        }
    }
